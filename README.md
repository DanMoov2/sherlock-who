# Sherlock Who?
A top-down detective game by @DannyT built in Phaser 3

# Setup
You’ll need to install a few things before you have a working copy of the project.

## 1. Clone this repo:

Navigate into your workspace directory.

Run:

```git clone git@bitbucket.org:DanMoov2/sherlock-who.git```

## 2. Install node.js and npm:

https://nodejs.org/en/


## 3. Install dependencies

Navigate to the cloned repo’s directory.

Run:

```npm install```


## 4. Run the development server:

Run:

```npm run dev```

This will run a server so you can run the game in a browser.

Open your browser and enter localhost:3000 into the address bar.

Also this will start a watch process, so you can change the source and the process will recompile and refresh the browser.


## Build for deployment:

Run:

```npm run deploy```

This will optimize and minimize the compiled bundle.


## Tile extrusion
Using tilesets in html can lead to bleeding issues. We use an NPM tool tile-extruder to solve this. For any tilesets run the following command

```npx tile-extruder --tileWidth 128 --tileHeight 128 --margin 0 --spacing 0 --input ./rawAssets/tilesets/Tileset.png --output ./assets/tilesets/Tileset.png```

see https://github.com/sporadic-labs/tile-extruder for more details.