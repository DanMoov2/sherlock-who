import 'phaser';
import BootScene from './scenes/BootScene';
import GameScene from './scenes/GameScene';
import TitleScene from './scenes/TitleScene';
import UIScene from './scenes/UIScene';
import { NinePatchPlugin, NinePatch } from '@koreez/phaser3-ninepatch';
import PhaserMatterCollisionPlugin from "phaser-matter-collision-plugin";

let config = {
    type: Phaser.WEBGL,              // Uses WebGL if available
    parent: 'content',              // container div to load into
    backgroundColor: '0x000000',
    width: window.innerWidth,       // grab the browser width
    height: window.innerHeight,     // and height
    physics: {
        default: 'matter',
        matter: {
            gravity: { y: 0, x:0 },
            debug: false
        }
    },
    plugins: {
        global: [ { key: 'NinePatchPlugin', plugin: NinePatchPlugin, start: true } ],
        scene: [
            {
              plugin: PhaserMatterCollisionPlugin,
              key: "matterCollision", // Where to store in Scene.Systems, e.g. scene.sys.matterCollision
              mapping: "matterCollision" // Where to store in the Scene, e.g. scene.matterCollision
            }
          ]
    },    
    scene: [
        BootScene,                  // First scene to load, sets up config and remains loaded
        TitleScene,                 // Show splash screen, instructions, "press space to play" etc
        GameScene,                  // Actual game scene
        UIScene                     // Score and other UI elements
    ]
};

let game = new Phaser.Game(config);

// listen for browser resize and invoke the game's resize function
window.addEventListener('resize', function (event) {
    game.resize(window.innerWidth, window.innerHeight);
}, false);