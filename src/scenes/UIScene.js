import CaseFileUI from "../casefile/CaseFileUI";
import InventoryUI from "../inventory/InventoryUI";
import CaseFeedbackUI from "../casefile/CaseFeedbackUI";
import ConversationUI from "../conversation/ConversationUI";

class UIScene extends Phaser.Scene {
    
    constructor() {
      super({
        key: 'UIScene'
      });
      this.conversationIndex = 0;
      this.currentConversation = [];
      this.inventory = [];
    }

    init(data) {
        this.caseFile = data.caseFile;
        this.script = data.script;
    }

    create() {
        this.gameHeight = this.sys.game.config.height;
        this.gameWidth = this.sys.game.config.width;

        /** 
         * Dialogue box
         **/
        this.conversationUI = new ConversationUI(this, this.gameWidth/2, this.gameHeight);
        
        /** 
         * Inventory
         **/

        // inventory icon
        this.inventoryIcon = this.add.image(70, this.gameHeight-70, 'atlas', 'ui/inventory-icon.png');
        
        let inventoryWidth = 177;
        let inventoryHeight = 234;
        this.inventoryUI = new InventoryUI(this,  inventoryWidth/2, this.gameHeight-this.inventoryIcon.height-inventoryHeight/2-30, inventoryWidth, inventoryHeight);
        this.inventoryUI.setVisible(false);
        this.children.add(this.inventoryUI);

        this.inventoryIcon.setInteractive();
        this.inventoryIcon.on('pointerdown', this.showInventory, this);
     
        /**
         * Case feedback
         */
        this.caseFeedbackUI = new CaseFeedbackUI(this, this.gameWidth/2, this.gameHeight/2, 400, 300);
        this.caseFeedbackUI.setVisible(false);
        this.children.add(this.caseFeedbackUI);

        /** 
         * Case file
         **/
        this.caseFileUI = new CaseFileUI(this, this.gameWidth/2, this.gameHeight/2, this.caseFile, 400, 750);
        this.caseFileUI.setVisible(false);
        this.children.add(this.caseFileUI);

        this.caseFileUI.on('deductionmade', (deduction) =>{
            this.caseFileUI.setVisible(false);
            this.caseFeedbackUI.on('continue', ()=> {
                this.scene.resume('GameScene');
            });
            this.caseFeedbackUI.setDeduction(deduction);
            this.caseFeedbackUI.setVisible(true);
        });

        // Case file icon
        this.caseFileIcon = this.add.image(this.inventoryIcon.width + 90, this.gameHeight-70, 'atlas', 'ui/case-icon.png');
        this.caseFileIcon.setInteractive();
        this.caseFileIcon.on('pointerdown', this.showCaseFile, this);

        //  Grab a reference to the Game Scene
        this.gameScene = this.scene.get('GameScene');
        this.gameScene.events.on('showDialogue', (conversationWith) => {
            this.scene.pause('GameScene');
            this.conversationUI.on('endConversation', ()=> {
                this.scene.resume('GameScene');
            });
            let conversationData = this.script.getConversationByContext(conversationWith);
            this.conversationUI.startConversation(conversationData);
        });

        this.gameScene.events.on('addToInvetory', (item) => {
            this.inventoryUI.addItem(item);
        });

        this.input.keyboard.on('keydown-I', this.showInventory, this);
        this.input.keyboard.on('keydown-C', this.showCaseFile, this);
    }

    showInventory() {
        this.inventoryUI.setVisible(!this.inventoryUI.visible);
    }

    showCaseFile() {
        let showCaseFile = !this.caseFileUI.visible;
        if(showCaseFile) {
            this.scene.pause('GameScene');
        } else {
            this.scene.resume('GameScene');
        }
        this.caseFileUI.setVisible(showCaseFile);
    }

}
export default UIScene;