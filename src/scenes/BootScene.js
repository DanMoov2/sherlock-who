class BootScene extends Phaser.Scene {
    
  constructor() {
      super({
        key: 'BootScene',
        pack: {
          files: [
              { type: 'image', key: 'logo', url: 'assets/images/moov2-logo.png' }
          ]
        }
      });
    }

    preload(){

      var progress = this.add.graphics();

      this.load.on('progress', (value) => {
          progress.clear();
          progress.fillStyle(0xffffff, 1);
          progress.fillRect(0, this.sys.game.config.width/2-60, this.sys.game.config.width * value, 60);
      });

      this.load.on('complete', function () {
          progress.destroy();
      });

      this.load.image('world', 'assets/tilesets/Tileset.png');
      this.load.image('background', 'assets/tilesets/Background.png');
      this.load.image('backgroundClouds', 'assets/tilesets/Cloud 1.png');
      this.load.tilemapTiledJSON('map', 'assets/tilemaps/demo-world-2.json');
      this.load.atlas('atlas', 'assets/images/atlas.png', 'assets/images/atlas.json');
      this.load.json('script', 'assets/json/script.json');
    }

    create(){
      this.scene.start('TitleScene');
    }
}

export default BootScene;
