import CaseFile from "../casefile/CaseFile";
import Script from "../script/Script";

class TitleScene extends Phaser.Scene {
    
    constructor() {
      super({
        key: 'TitleScene'
      });
    }

    /**
     * Called when browser window resizes
     */
    resize (width, height) {
      if (width === undefined) { width = this.sys.game.config.width; }
      if (height === undefined) { height = this.sys.game.config.height; }

      this.cameras.resize(width, height);
      this.physics.world.setBounds(0,0, width, height);
    }

    preload() {
    }

    create() {

      this.add.text(100, 100, 'Click to start', { fontFamily: 'Verdana, "Times New Roman", Tahoma, serif', fontSize: 64, color: '#ffffff' });

      // start game on click
      this.input.on('pointerup', (pointer) => {
        this.startGame();
      });
      this.startGame();
    }

    startGame() {
      let caseFile = new CaseFile();
      let script = new Script(this.cache.json.get('script'));
      this.scene.start('GameScene', {caseFile:caseFile});
      this.scene.start('UIScene', {
        caseFile:caseFile,
        script:script
      });
    }

    
}

export default TitleScene;
