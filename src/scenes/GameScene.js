import Player from '../player/Player';

class GameScene extends Phaser.Scene {
  
  constructor() {
    super({
      key: 'GameScene'
    });
  }

  init(data) {
    this.caseFile = data.caseFile;
  }

  create() {
    // setup tilemap
    const map = this.make.tilemap({ key: 'map' });
    const backgroundTileset = map.addTilesetImage('Background', 'background');
    const tileset = map.addTilesetImage('World', 'world', 128, 128, 1, 2);
    const background = map.createLayer('Background', backgroundTileset, 0,0);
    const ground = map.createLayer('Ground', tileset, 0, 0);
    const onGroundBelowPlayer = map.createLayer('On Ground Below', tileset, 0, 0);
    const onGroundAbovePlayer = map.createLayer('On Ground Above', tileset, 0, 0);
    // configure collision detection
    onGroundBelowPlayer.setCollisionByProperty({ collides: true });
    onGroundAbovePlayer.setCollisionByProperty({ collides: true });
    onGroundAbovePlayer.setDepth(10);
    this.matter.world.convertTilemapLayer(onGroundBelowPlayer);
    this.matter.world.convertTilemapLayer(onGroundAbovePlayer);
    this.matter.world.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    const debugGraphics = this.add.graphics().setAlpha(0.75);
    // onGroundAbovePlayer.renderDebug(debugGraphics, {
    //   tileColor: null, // Color of non-colliding tiles
    //   collidingTileColor: new Phaser.Display.Color(243, 134, 48, 255), // Color of colliding tiles
    //   faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Color of colliding face edges
    // });
    
    // setup objects on map
    var currentObject, mapObject, propsArray, frame;
    var objectLayer = map.getObjectLayer('Objects');
    this.interactables = [];
    this.collectables = [];

    for(var i = 0; i<objectLayer.objects.length; i++){
      mapObject = objectLayer.objects[i];
      if(mapObject.type === 'spawn') { // we deal with this later
        continue;
      }

      propsArray = mapObject.properties;
      frame = Phaser.Utils.Array.GetFirst(propsArray, 'name', 'frame');
      currentObject = this.matter.add.image(mapObject.x, mapObject.y, 'atlas', frame.value);
      
      currentObject.setName(mapObject.name);
      currentObject.setInteractive();
      currentObject.setStatic(true);

      // copy props from tiled to gameobject
      currentObject.setData('props', propsArray);
      this.interactables.push(currentObject);

      // add interactable objects to matter group
      if(mapObject.type === 'collide' || mapObject.type === 'collectable') {
        // handle touchevents on this item
        currentObject.on('pointerdown', (pointer, gameObject) => {
          this.checkInteraction();
        });

        // collectables array
        if(mapObject.type === 'collectable') {
          this.collectables.push(currentObject);
        }
      } else {
        currentObject.setSensor(true);
      }

      // places are invisible areas so we stretch a transparent png to match the size from the tilemap
      if(mapObject.type === 'place') {
        currentObject.setVisible(false);
        currentObject.displayWidth = mapObject.width;
        currentObject.displayHeight = mapObject.height;
        // Matter objects origin is in the middle so we reposition accordingly
        currentObject.setPosition(mapObject.x + mapObject.width/2, mapObject.y + mapObject.height/2);
      }
    };
    
    // player
    const spawnPoint = Phaser.Utils.Array.GetFirst(objectLayer.objects, 'name', 'start');
    this.player = new Player(this, spawnPoint.x, spawnPoint.y);
    this.player.setInteractables(this.interactables);
    this.player.events.on('discoveredplace', this.discoveredPlace, this);

    this.cameras.main.setBounds(0, 0, background.width, background.height);
    this.cameras.main.startFollow(this.player.sprite);
    
    this.input.keyboard.on('keyup-SPACE', this.onInteract, this);
    this.input.on('gameobjectdown', this.onInteract, this);
  }

  onInteract(event) {
    event.stopPropagation();
    
    let interactingWith = this.player.interactingWith;
    if(interactingWith === null) return;
    
    if(this.collectables.indexOf(interactingWith) > -1){
      this.collectItem(interactingWith);
    }        
    this.caseFile.addToCaseFile(interactingWith);
    this.startConversation();
  }

  discoveredPlace(place) {
    this.caseFile.addToCaseFile(place.gameObject);
    this.interactables.pop(place);
    this.children.remove(place.gameObject);
    this.matter.world.remove(place);
  }

  startConversation() {
    this.events.emit('showDialogue', this.player.interactingWith.frame.name);
  }

  collectItem(item){
    // Add to Inventory
    this.events.emit('addToInvetory', item.frame.name);
    var inventoryPoint = this.cameras.main.getWorldPoint(0, this.sys.game.config.height);
    var tween = this.tweens.add({
      targets: item,
      x: inventoryPoint.x,
      y: inventoryPoint.y,
      alpha: 0,
      ease: 'Power3',
      duration: 900,
      onComplete: () => {
        // this.interactiveObjects.remove(item, true);
      }
    });
  }
}

export default GameScene;