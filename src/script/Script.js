class Script {
    constructor (scriptData) {
        this.scriptData = scriptData;
    }

    getConversationByContext(contextId) {
        return this.scriptData[contextId];
    }
}

export default Script;