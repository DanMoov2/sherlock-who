import MultiKey from '../utils/MultiKey';

class Player {
    constructor (scene, x, y) {
        this.scene = scene;
        this.events = new Phaser.Events.EventEmitter();
        this.sprite = scene.matter.add.sprite(0, 0, 'atlas', 'character/idle-down/000.png', { inertia: Infinity });

        const { Body, Bodies } = Phaser.Physics.Matter.Matter; // Native Matter modules
        const mainBody = Bodies.rectangle(0, 0, 38, 40, { chamfer: { radius: 10 } });
        const sensorSize = {w:50, h:30};
        this.sensors = {
            top: Bodies.rectangle(0, -20, sensorSize.h, sensorSize.w, { isSensor: true }),
            bottom: Bodies.rectangle(0, 20, sensorSize.h, sensorSize.w, { isSensor: true }),
            left: Bodies.rectangle(-24, 0, sensorSize.w, sensorSize.h, { isSensor: true }),
            right: Bodies.rectangle(24, 0, sensorSize.w, sensorSize.h, { isSensor: true })
          };
        
        const compoundBody = Body.create({
            parts: [mainBody, this.sensors.top, this.sensors.bottom, this.sensors.left, this.sensors.right ]
        });
        // adjust physics body to ensure collissions are based around feet
        compoundBody.position.y -= 12; 
        compoundBody.positionPrev.y -= 12; 
        
        this.sprite
            .setExistingBody(compoundBody)
            .setPosition(x, y)
            .setFixedRotation();
        
        // Before matter's update, reset our record of what surfaces the player is touching.
        scene.matter.world.on("beforeupdate", this.hideIndicator, this);

        // use an active sensor to highlight interactive objects in the direction we're facing
        this.activeSensor = this.sensors.bottom;

        // icon to show something is interactable
        this.indicator = scene.add.image(0,0, 'atlas', 'ui/interactive-icon.png')
            .setVisible(false);

        // how fast we move
        this.movementSpeed = 8;

        // keep track of who/what we're interacting with
        this.interactingWith = null;

        this.sprite.anims.animationManager.create({ key: 'idle-down', frames: this.sprite.anims.animationManager.generateFrameNames('atlas', { prefix: 'character/idle-down/', end: 0, zeroPad: 3, suffix:'.png' }), repeat: -1 });
        this.sprite.anims.animationManager.create({ key: 'walk-left', frames: this.sprite.anims.animationManager.generateFrameNames('atlas', { prefix: 'character/walk-left/', end: 7, zeroPad: 3, suffix:'.png' }), repeat: -1 });
        this.sprite.anims.animationManager.create({ key: 'walk-up', frames: this.sprite.anims.animationManager.generateFrameNames('atlas', { prefix: 'character/walk-up/', end: 7, zeroPad: 3, suffix:'.png' }), repeat: -1 });
        this.sprite.anims.animationManager.create({ key: 'walk-right', frames: this.sprite.anims.animationManager.generateFrameNames('atlas', { prefix: 'character/walk-right/', end: 7, zeroPad: 3, suffix:'.png' }), repeat: -1 });
        this.sprite.anims.animationManager.create({ key: 'walk-down', frames: this.sprite.anims.animationManager.generateFrameNames('atlas', { prefix: 'character/walk-down/', end: 7, zeroPad: 3, suffix:'.png' }), repeat: -1 });

        this.cursors = scene.input.keyboard.createCursorKeys();
        this.wasd = scene.input.keyboard.addKeys({
            up: Phaser.Input.Keyboard.KeyCodes.W,
            down: Phaser.Input.Keyboard.KeyCodes.S,
            left: Phaser.Input.Keyboard.KeyCodes.A,
            right: Phaser.Input.Keyboard.KeyCodes.D
        });
        this.activePointer = scene.input.activePointer;

        scene.input.on('pointerdown', (pointer, gameObject) => {
            this.activePointer = true;          
        });
        scene.input.on('pointerup', (pointer, gameObject) => {
            this.activePointer = false;
        });

        scene.input.on('gameobjectdown', (pointer, gameObject) => {
            this.activePointer = false;
        });

        this.idleState = 'idle-down';
        this.sprite.play(this.idleState);
        this.scene.events.on("update", this.update, this);
    }

    setInteractables(interactables) {
        // Collision detection
        this.scene.matterCollision.addOnCollideStart({
            objectA: [this.sensors.top, this.sensors.bottom, this.sensors.left, this.sensors.right],
            objectB: interactables,
            callback: this.onSensorCollide,
            context: this
        });
        this.scene.matterCollision.addOnCollideActive({
            objectA: [this.sensors.top, this.sensors.bottom, this.sensors.left, this.sensors.right],
            objectB: interactables,
            callback: this.onSensorCollide,
            context: this
        });
    }

    hideIndicator() {
        this.indicator.setVisible(false);
        this.interactingWith = null;
    }

    onSensorCollide({ bodyA, bodyB, pair }) {
        // is it a zone?
        var type = Phaser.Utils.Array.GetFirst(bodyB.gameObject.data.values.props, 'name', 'caseFileType');
        console.log(type)
        if(type.value === 'place') { // remove places once discovered
            this.events.emit('discoveredplace', bodyB);
            return;
        }
        if (bodyA === this.activeSensor) {
            this.indicator.setVisible(true);
        }
        this.interactingWith = bodyB.gameObject;
    }

    update (time, delta) {
        this.sprite.setVelocity(0);

        // keyboard controls
        if (this.cursors.up.isDown || this.wasd.up.isDown) {
            this.sprite.setVelocityY(-this.movementSpeed);
        }
        else if (this.cursors.down.isDown || this.wasd.down.isDown) {
            this.sprite.setVelocityY(this.movementSpeed);
        }

        if (this.cursors.left.isDown || this.wasd.left.isDown) {
            this.sprite.setVelocityX(-this.movementSpeed);
        }
        else if (this.cursors.right.isDown || this.wasd.right.isDown) {
            this.sprite.setVelocityX(this.movementSpeed);
        }

        // pointer (mouse/touch) controls
        var angle = 0;
        if(this.activePointer && this.scene.input.activePointer.isDown && this.scene.input.activePointer.leftButtonDown()) {
            // get angle between player and touch point
            angle = Phaser.Math.RadToDeg(Phaser.Math.Angle.Between(this.x, this.y, this.scene.input.activePointer.worldX, this.scene.input.activePointer.worldY));

            // up/down
            if(angle > -150 && angle < -30) {
                this.sprite.setVelocityY(-this.movementSpeed);
            } else if (angle > 30 && angle < 150) {
                this.sprite.setVelocityY(this.movementSpeed);
            }
            // left/right
            if (angle > 120 || angle < -120) {
                this.sprite.setVelocityX(-this.movementSpeed);
            } else if(angle > -60 && angle < 60) {
                this.sprite.setVelocityX(this.movementSpeed);
            }
        }

        if(this.sprite.body.velocity.x > 0) {
            this.sprite.play('walk-right', true);
            this.idleState = 'idle-right';
            this.activeSensor = this.sensors.right;
        } else if(this.sprite.body.velocity.x < 0) {
            this.sprite.play('walk-left', true);
            this.idleState = 'idle-left';
            this.activeSensor = this.sensors.left;
        }
        if(this.sprite.body.velocity.x == 0) {
            if(this.sprite.body.velocity.y < 0) {
                this.sprite.play('walk-up', true);
                this.idleState = 'idle-up';
                this.activeSensor = this.sensors.top;
            } else if(this.sprite.body.velocity.y > 0) {
                this.sprite.play('walk-down', true);
                this.idleState = 'idle-down';
                this.activeSensor = this.sensors.bottom;
            }
            if(this.sprite.body.velocity.y == 0) {
                this.sprite.play(this.idleState, true);
            }
        }

        this.indicator.setPosition(this.sprite.x+25, this.sprite.y-20);
    }
}

export default Player;