class InventoryUI extends Phaser.GameObjects.Container {
    constructor (scene, x, y, width, height) {
        super(scene, x, y);
        this.inventory = [];

        let backgroundBox = scene.add.ninePatch(
            0, 0,   // this is the starting x/y location
            width, height,   // the width and height of your object
            'atlas',
            'ui/blue-panel.png',
            {
                top: 8, // Amount of pixels for top
                bottom: 8, // Amount of pixels for bottom
                left: 8, // Amount of pixels for left
                right: 8 // Amount of pixels for right
            }
        );
        this.add(backgroundBox);

        let inventoryGroup = scene.add.group();
        this.slots = inventoryGroup.createMultiple({
            key: 'atlas',
            frame: 'ui/grey-box.png',
            repeat: 11
        });
        Phaser.Actions.GridAlign(this.slots, {
            width: 3,
            height: 4,
            cellWidth: 56,
            cellHeight: 56,
            position: Phaser.Display.Align.TOP_LEFT,
            x:-54, 
            y:-height/3
        });
        this.add(this.slots);
    }

    addItem(item) {
        var slot = this.slots[this.inventory.length];
        this.inventory.push(item);
        this.add(this.scene.add.sprite(slot.x, slot.y, 'atlas', item));
    }
}
export default InventoryUI;