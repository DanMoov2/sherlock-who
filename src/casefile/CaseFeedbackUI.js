class CaseFeedbackUI extends Phaser.GameObjects.Container {
    constructor (scene, x, y, width=600, height=800) {
        super(scene, x, y);

        // Background box
        let backgroundBox = scene.add.ninePatch(
            0, 0,   // this is the starting x/y location
            width, height,   // the width and height of your object
            'atlas',
            'ui/blue-panel.png',
            {
                top: 8, // Amount of pixels for top
                bottom: 8, // Amount of pixels for bottom
                left: 8, // Amount of pixels for left
                right: 8 // Amount of pixels for right
            }
        );
        this.add(backgroundBox);

        // Title
        this.title = scene.make.text({
            text: 'Incorrect deduction',
            style: {
                fontFamily: '"Arial"',
                fontSize: '25px',
                fontStyle: 'bold',
                fill: 'white'
            }
        });
        this.title.setOrigin(0.5);
        this.title.setPosition(0, -height/2+40);
        this.add(this.title);

        // Explanation
        this.description = scene.make.text({
            text: 'You got it wrong bud',
            style: {
                fontFamily: '"Arial"',
                fontSize: '16px',
                fill: 'white',
                wordWrap: { width: width }
            }
        });
        this.description.setOrigin(0.5);
        this.description.setPosition(0, this.title.y+40);
        this.add(this.description);

        // button
        let button = scene.add.sprite(0, 0, 'atlas', 'ui/green-button.png');    
        button.setInteractive();
        button.setPosition(0, height/2-60);
        this.add(button);
        this.buttonLabel = scene.make.text({
            text: 'Keep investigating',
            style: {
                fontFamily: '"Arial"',
                fontSize: '18px',
                fontStyle: 'bold',
                fill: 'white'
            }
        });
        this.buttonLabel.setOrigin(0.5);
        this.buttonLabel.setPosition(button.x, button.y-2);
        this.add(this.buttonLabel);
        button.on('pointerdown', () => {
            this.setVisible(false);
            this.emit('continue');
        })
    }

    setDeduction(deduction) {
        if(deduction.success) {
            this.title.setText('Success!');
            this.description.setText('Well done! You\'ve cracked the case');
            this.buttonLabel.setText('Finish');
        } else {
            this.title.setText("Incorrect");
            this.description.setText('Unfortunately you haven\'t cracked the case.\n' + deduction.correctCount + ' of your guesses are right so far but you may also have made some incorrect assertions...');
            this.buttonLabel.setText('Keep investigating');
        }
        
    }
}
export default CaseFeedbackUI;