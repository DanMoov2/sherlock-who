import AlertBox from '../uicomponents/AlertBox';

class CaseFileUI extends Phaser.GameObjects.Container {
    constructor (scene, x, y, caseFile, width=600, height=800) {
        super(scene, x, y);
        this.slotsPerType = 8;
        this.itemSlotsAvailable = this.slotsPerType;
        this.characterSlotsAvailable = this.slotsPerType;
        this.placeSlotsAvailable = this.slotsPerType;
        this.caseFile = caseFile;
        this.caseFile.events.on('itemadded', this.itemAdded, this);
        this.selected = [];

        // Background box
        let backgroundBox = scene.add.ninePatch(
            0, 0,   // this is the starting x/y location
            width, height,   // the width and height of your object
            'atlas',
            'ui/blue-panel.png',
            {
                top: 8, // Amount of pixels for top
                bottom: 8, // Amount of pixels for bottom
                left: 8, // Amount of pixels for left
                right: 8 // Amount of pixels for right
            }
        );
        this.add(backgroundBox);

        // Use a group to make layout easier
        let caseFileUI = scene.add.group();

        // Titles
        let peopleTitle = this.createTitle(scene, 'People');
        let itemsTitle = this.createTitle(scene, 'Items');
        let placesTitle = this.createTitle(scene, 'Places');
        caseFileUI.addMultiple([peopleTitle, itemsTitle, placesTitle]);

        // Empty boxes
        this.slots = caseFileUI.createMultiple({
            key: 'atlas',
            frame: 'ui/grey-box.png',
            repeat: this.slotsPerType * 3 - 1
        });

        // arrange in grid
        let allChildren = caseFileUI.getChildren();
        Phaser.Actions.GridAlign(allChildren, {
            width: 3,
            height: 10,
            cellWidth: width/3,
            cellHeight: height/10,
            position: Phaser.Display.Align.CENTER,
            x:-width/3, 
            y:-height/3 - (height/10) 
        });
        this.add(allChildren);

        /**
         * Solve it button
         */
        let button = scene.add.sprite(0, height/2-40, 'atlas', 'ui/green-button.png');
        button.setInteractive();
        this.add(button);
        let text = this.createTitle(scene, 'Solve it!');
        text.setOrigin(0.5);
        text.setPosition(button.x, button.y-4);
        this.add(text);

        button.on('pointerdown', this.solveIt, this);

    }

    createTitle(scene, text) {
        return scene.make.text({
            text: text,
            style: {
                fontFamily: '"Arial"',
                fontSize: '25px',
                fontStyle: 'bold',
                fill: 'white'
            }
        });
    }

    itemAdded(typeAdded, item) {
        switch(typeAdded.value){
            case 'people':
                this.addItem(item, this.characterSlotsAvailable, 0);
                this.characterSlotsAvailable--;
                break;
            case 'item':
                this.addItem(item, this.itemSlotsAvailable, 1.0);
                this.itemSlotsAvailable--;
                break;
            case 'place':
                this.addItem(item, this.placeSlotsAvailable, 2.0);
                this.placeSlotsAvailable--;
                break;
        }
    }

    addItem(item, availableSlots, slotOffset) {
        // get next empty slot (use offest to find the right column)
        let slot = this.slots[slotOffset + Math.ceil((this.slotsPerType - availableSlots)/3.0) * 3];
        // add sprite over slot
        let sprite = this.scene.add.sprite(slot.x+30, slot.y, 'atlas', item.frame.name);
        this.add(sprite);
        sprite.setInteractive();
        sprite.on('pointerdown', (event) => {
            if(this.selected.indexOf(item) >= 0) {
                slot.setFrame('ui/grey-box.png');
                this.selected.splice(this.selected.indexOf(item), 1);
            } else {
                slot.setFrame('ui/grey-box-border.png');
                this.selected.push(item);
            }
        });
    }

    solveIt(){
        // validate
        if(!this.validate()) {
            let alert = new AlertBox(this.scene, 0, 0, 300, 200, 'You must select at least one character, one item and one place.');
            this.scene.children.add(alert);
            this.add(alert);
            return;
        }
        // success check
        let correctCount = 0;
        let wrongCount = 0;
        //TODO: Make dynamic
        let solution = ['crying-child', 'chicken', 'chicken-coop'];
        for(let i=0; i < this.selected.length; i++) {
            if(solution.indexOf(this.selected[i].name) == -1) {
                wrongCount++;
                continue;
            }
            correctCount++;
        }
        let deduction = {
            success:true,
            correctCount:correctCount
        }
        if(wrongCount > 0 || correctCount < solution.length) {
            deduction.success = false;
        }
        this.emit('deductionmade', deduction);
        
    }

    validate() {
        let hasPeople = false;
        let hasItems = false;
        let hasPlaces = false;
        for(let i=0; i < this.selected.length; i++) {
            let current = Phaser.Utils.Array.GetFirst(this.selected[i].data.values.props, 'name', 'caseFileType');
            switch(current.value){
                case 'people':
                    hasPeople = true;
                break;
                case 'item':
                    hasItems = true;
                break;
                case 'place':
                    hasPlaces = true;
                break;
            }
        }
        return hasPeople && hasItems && hasPlaces;
    }
}
export default CaseFileUI;