class CaseFile {
    constructor () {
        this.characters = [];
        this.items = [];
        this.places = [];
        this.events = new Phaser.Events.EventEmitter();
    }

    addToCaseFile(item) {
        var typeAdded = Phaser.Utils.Array.GetFirst(item.data.values.props, 'name', 'caseFileType');

        switch(typeAdded.value){
            case 'people':
                if(this.characters.indexOf(item) > -1) {
                    return;
                }
                this.characters.push(item);
                break;
            case 'item':
                if(this.items.indexOf(item) > -1) {
                    return;
                }
                this.items.push(item);
                break;
            case 'place':
                if(this.places.indexOf(item) > -1) {
                    return;
                }
                this.places.push(item);
                break;
        }

        this.events.emit('itemadded', typeAdded, item);
    }
}
export default CaseFile;