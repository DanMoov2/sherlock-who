class AlertBox extends Phaser.GameObjects.Container {
    constructor (scene, x, y, width=400, height=150, text='Alert') {
        super(scene, x, y);
        
        // Background box
        let backgroundBox = scene.add.ninePatch(
            0, 0,   // this is the starting x/y location
            width, height,   // the width and height of your object
            'atlas',
            'ui/blue-panel.png',
            {
                top: 8, // Amount of pixels for top
                bottom: 8, // Amount of pixels for bottom
                left: 8, // Amount of pixels for left
                right: 8 // Amount of pixels for right
            }
        );
        this.add(backgroundBox);

        // Explanation
        this.description = scene.make.text({
            text: text,
            style: {
                fontFamily: '"Arial"',
                fontSize: '16px',
                fill: 'white',
                wordWrap: { width: width }
            }
        });
        this.description.setOrigin(0.5, 0);
        this.description.setPosition(0, -height/2 + 20);
        this.add(this.description);

        // button
        let button = scene.add.sprite(0, 0, 'atlas', 'ui/green-button.png');    
        button.setInteractive();
        button.setPosition(0, height/2-60);
        this.add(button);
        this.buttonLabel = scene.make.text({
            text: 'OK',
            style: {
                fontFamily: '"Arial"',
                fontSize: '18px',
                fontStyle: 'bold',
                fill: 'white'
            }
        });
        this.buttonLabel.setOrigin(0.5);
        this.buttonLabel.setPosition(button.x, button.y-2);
        this.add(this.buttonLabel);
        button.on('pointerdown', () => {
            this.setVisible(false);
            this.destroy();
        })
    }
}
export default AlertBox;