import SpeechBubble from "./SpeechBubble";

class ConversationUI extends Phaser.GameObjects.Container {
    constructor (scene, x, y, width=800) {
        super(scene, x, y);
        scene.children.add(this);

        // store dimensions for layout
        this.width = width;

        // hide until needed
        this.setVisible(false);
        this.setActive(false);

        // Positions for avatars
        this.avatarPositions = [
         new Phaser.Math.Vector2(-this.width/2, -100),
         new Phaser.Math.Vector2(this.width/2, -100)
        ];

        // Array of speech bubbles
        this.speechBubbles = [];

        // Array of characters in conversation
        this.characters = [];

        // TODO: move this into gamestate somewhere
        this.setVars = [];
    }

    startConversation(conversationData) {
        this.conversationData = conversationData;
        this.conversationIndex = 0;
        this.createAvatars();
        this.setCurrentSnippet();
        this.setVisible(true);
        this.setActive(true);
        this.scene.input.keyboard.on('keyup-SPACE', this.progressChat, this);
    }

    createAvatars() {
        // find all avatars referenced
        for(let i = 0; i < this.conversationData.length; i++) {
            let snippet = this.conversationData[i];
            if(snippet.hasOwnProperty('img')) {
                let avatarPath = snippet.img;
                let avatarIndex = this.characters.findIndex(char => {return char.frame.name === avatarPath});
                if(avatarIndex > -1) {
                    continue;
                }
                let posIndex = this.characters.length;
                let pos = this.avatarPositions[posIndex];
                let newCharacterImage = this.scene.add.image(posIndex%2==0 ? -500 : 500, pos.y, 'atlas', avatarPath)
                    .setAlpha(0);
                this.add(newCharacterImage);
                this.characters.push(newCharacterImage);
                this.scene.tweens.add({
                    targets: newCharacterImage,
                    x: pos.x,
                    alpha: 1,
                    ease: 'Sine.easeOut',
                    duration: 300
                });
            }
        }
    }

    setCurrentSnippet() {
        // grab the current line of text
        let currentSnippet = this.conversationData[this.conversationIndex];

        // look up who is talking
        let pos = this.avatarPositions[0];
        let alignLeft = true;
        
        // check if character is left or right hand side
        if(currentSnippet.hasOwnProperty('img')) {
            let characterIndex = this.characters.findIndex(char => {return char.frame.name === currentSnippet.img});
            pos = this.avatarPositions[characterIndex];
            alignLeft = characterIndex%2 === 0;
        }

        // create a new speech bubble
        let options = currentSnippet.hasOwnProperty('options') ? currentSnippet.options : null;
        let textToAvatarOffset = alignLeft ? 50 : -50;
        let bubble = new SpeechBubble(this.scene, pos.x + textToAvatarOffset, 10, currentSnippet.text, alignLeft, true, options);
        this.add(bubble);
        this.speechBubbles.push(bubble);

        // handle options
        if(options !== null) {
            bubble.once('optionselected', this.optionSelected, this);
            this.scene.input.keyboard.off('keyup-SPACE', this.progressChat, this);
        }

        // move everything else up
        let bottomBubbbleY = 0;
        for(let i=this.speechBubbles.length-1; i>=0; i--) {
            let bubble = this.speechBubbles[i];
            this.scene.tweens.add({
                targets: [bubble],
                y: -bottomBubbbleY,
                alpha: (i<this.speechBubbles.length-1) ? 0.7 : 1, // fade out everything but the latest
                ease: 'Sine.easeOut',
                duration: 300
            });
            bottomBubbbleY += this.speechBubbles[i].bubble.height + 10;
        }

        // remove oldest
        const keepTextVisibleCount = 2;
        if(this.speechBubbles.length > keepTextVisibleCount){
            let topBubble = this.speechBubbles[0];
            // fade out and up
            this.scene.tweens.add({
                targets: [topBubble],
                alpha: 0,
                ease: 'Sine.easeOut',
                duration: 300,
                onComplete: (tween, targets) => this.remove(targets[0], true) // remove from view
            });
            // remove from array
            this.speechBubbles.splice(0, 1);
        }
    }

    progressChat() {
        console.log('progressing conversation');
        this.conversationIndex++;
        if(this.conversationIndex >= this.conversationData.length) {
            this.endConversation();
            return;
        }
        this.setCurrentSnippet();
    }

    endConversation() {
        let i = 0;
        for(i=this.speechBubbles.length-1; i>=0; i--) { 
            this.remove(this.speechBubbles[i], true);
            this.speechBubbles.splice(i,1);
        }
        this.speechBubbles = [];

        this.scene.tweens.add({
            targets: this.characters,
            x: {
                getEnd: function (target, key, value) {
                    return value < 0 ? -500 : 500;
                }
            },
            alpha: 0,
            ease: 'Sine.easeOut',
            duration: 300,
            onComplete: (tween, targets) => {
                this.remove(targets, true);
                this.setVisible(false);
                this.setActive(false);
            }
        });        
        this.characters = [];

        this.scene.input.keyboard.off('keyup-SPACE', this.progressChat, this);
        this.emit('endConversation');
    }

    optionSelected(selectedOption) {
        if(selectedOption.hasOwnProperty('setVar')){
            this.setVars.push(selectedOption.setVar);
        }
        
        if(selectedOption.hasOwnProperty('goto')){
            let goto = Number(selectedOption.goto);
            if(goto == -1){
                this.endConversation();
                return;
            }
            this.conversationIndex = goto-1; // -1 because we immediately add one back on in progress chat
        }
        this.scene.input.keyboard.on('keyup-SPACE', this.progressChat, this);
        this.progressChat();
    }
}
export default ConversationUI;