<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.10.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../assets/images/atlas.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <true/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../images/avatars/player.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,50,59,100</rect>
                <key>scale9Paddings</key>
                <rect>30,50,59,100</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/character/cast-down/000.png</key>
            <key type="filename">../images/character/cast-down/001.png</key>
            <key type="filename">../images/character/cast-down/002.png</key>
            <key type="filename">../images/character/cast-down/003.png</key>
            <key type="filename">../images/character/cast-down/004.png</key>
            <key type="filename">../images/character/cast-down/005.png</key>
            <key type="filename">../images/character/cast-down/006.png</key>
            <key type="filename">../images/character/cast-left/000.png</key>
            <key type="filename">../images/character/cast-left/001.png</key>
            <key type="filename">../images/character/cast-left/002.png</key>
            <key type="filename">../images/character/cast-left/003.png</key>
            <key type="filename">../images/character/cast-left/004.png</key>
            <key type="filename">../images/character/cast-left/005.png</key>
            <key type="filename">../images/character/cast-left/006.png</key>
            <key type="filename">../images/character/cast-right/000.png</key>
            <key type="filename">../images/character/cast-right/001.png</key>
            <key type="filename">../images/character/cast-right/002.png</key>
            <key type="filename">../images/character/cast-right/003.png</key>
            <key type="filename">../images/character/cast-right/004.png</key>
            <key type="filename">../images/character/cast-right/005.png</key>
            <key type="filename">../images/character/cast-right/006.png</key>
            <key type="filename">../images/character/cast-up/000.png</key>
            <key type="filename">../images/character/cast-up/001.png</key>
            <key type="filename">../images/character/cast-up/002.png</key>
            <key type="filename">../images/character/cast-up/003.png</key>
            <key type="filename">../images/character/cast-up/004.png</key>
            <key type="filename">../images/character/cast-up/005.png</key>
            <key type="filename">../images/character/cast-up/006.png</key>
            <key type="filename">../images/character/hurt/000.png</key>
            <key type="filename">../images/character/hurt/001.png</key>
            <key type="filename">../images/character/hurt/002.png</key>
            <key type="filename">../images/character/hurt/003.png</key>
            <key type="filename">../images/character/hurt/004.png</key>
            <key type="filename">../images/character/hurt/005.png</key>
            <key type="filename">../images/character/idle-down/000.png</key>
            <key type="filename">../images/character/idle-left/000.png</key>
            <key type="filename">../images/character/idle-right/000.png</key>
            <key type="filename">../images/character/idle-up/000.png</key>
            <key type="filename">../images/character/shoot-down/000.png</key>
            <key type="filename">../images/character/shoot-down/001.png</key>
            <key type="filename">../images/character/shoot-down/002.png</key>
            <key type="filename">../images/character/shoot-down/003.png</key>
            <key type="filename">../images/character/shoot-down/004.png</key>
            <key type="filename">../images/character/shoot-down/005.png</key>
            <key type="filename">../images/character/shoot-down/006.png</key>
            <key type="filename">../images/character/shoot-down/007.png</key>
            <key type="filename">../images/character/shoot-down/008.png</key>
            <key type="filename">../images/character/shoot-down/009.png</key>
            <key type="filename">../images/character/shoot-down/010.png</key>
            <key type="filename">../images/character/shoot-down/011.png</key>
            <key type="filename">../images/character/shoot-down/012.png</key>
            <key type="filename">../images/character/shoot-left/000.png</key>
            <key type="filename">../images/character/shoot-left/001.png</key>
            <key type="filename">../images/character/shoot-left/002.png</key>
            <key type="filename">../images/character/shoot-left/003.png</key>
            <key type="filename">../images/character/shoot-left/004.png</key>
            <key type="filename">../images/character/shoot-left/005.png</key>
            <key type="filename">../images/character/shoot-left/006.png</key>
            <key type="filename">../images/character/shoot-left/007.png</key>
            <key type="filename">../images/character/shoot-left/008.png</key>
            <key type="filename">../images/character/shoot-left/009.png</key>
            <key type="filename">../images/character/shoot-left/010.png</key>
            <key type="filename">../images/character/shoot-left/011.png</key>
            <key type="filename">../images/character/shoot-left/012.png</key>
            <key type="filename">../images/character/shoot-right/000.png</key>
            <key type="filename">../images/character/shoot-right/001.png</key>
            <key type="filename">../images/character/shoot-right/002.png</key>
            <key type="filename">../images/character/shoot-right/003.png</key>
            <key type="filename">../images/character/shoot-right/004.png</key>
            <key type="filename">../images/character/shoot-right/005.png</key>
            <key type="filename">../images/character/shoot-right/006.png</key>
            <key type="filename">../images/character/shoot-right/007.png</key>
            <key type="filename">../images/character/shoot-right/008.png</key>
            <key type="filename">../images/character/shoot-right/009.png</key>
            <key type="filename">../images/character/shoot-right/010.png</key>
            <key type="filename">../images/character/shoot-right/011.png</key>
            <key type="filename">../images/character/shoot-right/012.png</key>
            <key type="filename">../images/character/shoot-up/000.png</key>
            <key type="filename">../images/character/shoot-up/001.png</key>
            <key type="filename">../images/character/shoot-up/002.png</key>
            <key type="filename">../images/character/shoot-up/003.png</key>
            <key type="filename">../images/character/shoot-up/004.png</key>
            <key type="filename">../images/character/shoot-up/005.png</key>
            <key type="filename">../images/character/shoot-up/006.png</key>
            <key type="filename">../images/character/shoot-up/007.png</key>
            <key type="filename">../images/character/shoot-up/008.png</key>
            <key type="filename">../images/character/shoot-up/009.png</key>
            <key type="filename">../images/character/shoot-up/010.png</key>
            <key type="filename">../images/character/shoot-up/011.png</key>
            <key type="filename">../images/character/shoot-up/012.png</key>
            <key type="filename">../images/character/slash-down/000.png</key>
            <key type="filename">../images/character/slash-down/001.png</key>
            <key type="filename">../images/character/slash-down/002.png</key>
            <key type="filename">../images/character/slash-down/003.png</key>
            <key type="filename">../images/character/slash-down/004.png</key>
            <key type="filename">../images/character/slash-down/005.png</key>
            <key type="filename">../images/character/slash-left/000.png</key>
            <key type="filename">../images/character/slash-left/001.png</key>
            <key type="filename">../images/character/slash-left/002.png</key>
            <key type="filename">../images/character/slash-left/003.png</key>
            <key type="filename">../images/character/slash-left/004.png</key>
            <key type="filename">../images/character/slash-left/005.png</key>
            <key type="filename">../images/character/slash-right/000.png</key>
            <key type="filename">../images/character/slash-right/001.png</key>
            <key type="filename">../images/character/slash-right/002.png</key>
            <key type="filename">../images/character/slash-right/003.png</key>
            <key type="filename">../images/character/slash-right/004.png</key>
            <key type="filename">../images/character/slash-right/005.png</key>
            <key type="filename">../images/character/slash-up/000.png</key>
            <key type="filename">../images/character/slash-up/001.png</key>
            <key type="filename">../images/character/slash-up/002.png</key>
            <key type="filename">../images/character/slash-up/003.png</key>
            <key type="filename">../images/character/slash-up/004.png</key>
            <key type="filename">../images/character/slash-up/005.png</key>
            <key type="filename">../images/character/thrust-down/000.png</key>
            <key type="filename">../images/character/thrust-down/001.png</key>
            <key type="filename">../images/character/thrust-down/002.png</key>
            <key type="filename">../images/character/thrust-down/003.png</key>
            <key type="filename">../images/character/thrust-down/004.png</key>
            <key type="filename">../images/character/thrust-down/005.png</key>
            <key type="filename">../images/character/thrust-down/006.png</key>
            <key type="filename">../images/character/thrust-down/007.png</key>
            <key type="filename">../images/character/thrust-left/000.png</key>
            <key type="filename">../images/character/thrust-left/001.png</key>
            <key type="filename">../images/character/thrust-left/002.png</key>
            <key type="filename">../images/character/thrust-left/003.png</key>
            <key type="filename">../images/character/thrust-left/004.png</key>
            <key type="filename">../images/character/thrust-left/005.png</key>
            <key type="filename">../images/character/thrust-left/006.png</key>
            <key type="filename">../images/character/thrust-left/007.png</key>
            <key type="filename">../images/character/thrust-right/000.png</key>
            <key type="filename">../images/character/thrust-right/001.png</key>
            <key type="filename">../images/character/thrust-right/002.png</key>
            <key type="filename">../images/character/thrust-right/003.png</key>
            <key type="filename">../images/character/thrust-right/004.png</key>
            <key type="filename">../images/character/thrust-right/005.png</key>
            <key type="filename">../images/character/thrust-right/006.png</key>
            <key type="filename">../images/character/thrust-right/007.png</key>
            <key type="filename">../images/character/thurst-up/000.png</key>
            <key type="filename">../images/character/thurst-up/001.png</key>
            <key type="filename">../images/character/thurst-up/002.png</key>
            <key type="filename">../images/character/thurst-up/003.png</key>
            <key type="filename">../images/character/thurst-up/004.png</key>
            <key type="filename">../images/character/thurst-up/005.png</key>
            <key type="filename">../images/character/thurst-up/006.png</key>
            <key type="filename">../images/character/thurst-up/007.png</key>
            <key type="filename">../images/character/walk-down/000.png</key>
            <key type="filename">../images/character/walk-down/001.png</key>
            <key type="filename">../images/character/walk-down/002.png</key>
            <key type="filename">../images/character/walk-down/003.png</key>
            <key type="filename">../images/character/walk-down/004.png</key>
            <key type="filename">../images/character/walk-down/005.png</key>
            <key type="filename">../images/character/walk-down/006.png</key>
            <key type="filename">../images/character/walk-down/007.png</key>
            <key type="filename">../images/character/walk-left/000.png</key>
            <key type="filename">../images/character/walk-left/001.png</key>
            <key type="filename">../images/character/walk-left/002.png</key>
            <key type="filename">../images/character/walk-left/003.png</key>
            <key type="filename">../images/character/walk-left/004.png</key>
            <key type="filename">../images/character/walk-left/005.png</key>
            <key type="filename">../images/character/walk-left/006.png</key>
            <key type="filename">../images/character/walk-left/007.png</key>
            <key type="filename">../images/character/walk-right/000.png</key>
            <key type="filename">../images/character/walk-right/001.png</key>
            <key type="filename">../images/character/walk-right/002.png</key>
            <key type="filename">../images/character/walk-right/003.png</key>
            <key type="filename">../images/character/walk-right/004.png</key>
            <key type="filename">../images/character/walk-right/005.png</key>
            <key type="filename">../images/character/walk-right/006.png</key>
            <key type="filename">../images/character/walk-right/007.png</key>
            <key type="filename">../images/character/walk-up/000.png</key>
            <key type="filename">../images/character/walk-up/001.png</key>
            <key type="filename">../images/character/walk-up/002.png</key>
            <key type="filename">../images/character/walk-up/003.png</key>
            <key type="filename">../images/character/walk-up/004.png</key>
            <key type="filename">../images/character/walk-up/005.png</key>
            <key type="filename">../images/character/walk-up/006.png</key>
            <key type="filename">../images/character/walk-up/007.png</key>
            <key type="filename">../images/inventory/chicken.png</key>
            <key type="filename">../images/misc/oil-spill.png</key>
            <key type="filename">../images/npc/idle-down/000.png</key>
            <key type="filename">../images/places/alleyway.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/misc/blank.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,2,4,4</rect>
                <key>scale9Paddings</key>
                <rect>2,2,4,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/places/chicken-coop.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,48,101,96</rect>
                <key>scale9Paddings</key>
                <rect>50,48,101,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/ui/blue-cross.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,9,19,18</rect>
                <key>scale9Paddings</key>
                <rect>10,9,19,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/ui/blue-panel.png</key>
            <key type="filename">../images/ui/case-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/ui/dialogue-box.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,19,50,37</rect>
                <key>scale9Paddings</key>
                <rect>25,19,50,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/ui/green-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,12,95,25</rect>
                <key>scale9Paddings</key>
                <rect>48,12,95,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/ui/grey-box-border.png</key>
            <key type="filename">../images/ui/grey-box.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,25,25</rect>
                <key>scale9Paddings</key>
                <rect>12,12,25,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/ui/interactive-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,3,6,6</rect>
                <key>scale9Paddings</key>
                <rect>3,3,6,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/ui/inventory-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,85,176,170</rect>
                <key>scale9Paddings</key>
                <rect>88,85,176,170</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/ui/option-highlight.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>1,0</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,27,27</rect>
                <key>scale9Paddings</key>
                <rect>14,14,27,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../images/character</filename>
            <filename>../images/npc</filename>
            <filename>../images/avatars</filename>
            <filename>../images/ui</filename>
            <filename>../images/misc</filename>
            <filename>../images/inventory</filename>
            <filename>../images/places</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array>
            <string>phaser3-exporter-beta</string>
        </array>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
